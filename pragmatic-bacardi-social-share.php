<?php
/**
 * The plugin bootstrap file
 *
 * @wordpress-plugin
 * Plugin Name:       Pragmatic Bacardi Social Share
 * Plugin URI:        http://pragmatic-web.co.uk/plugins/
 * Description:       Configure and display social share icons
 * Version:           1.0
 * Author:            Pragmatic Web Limited
 * Author URI:        http://pragmatic-web.co.uk
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pragmatic-bacardi-social-share
 * Domain Path:       /languages
 * Bitbucket Plugin URI: https://bitbucket.org/pragmaticweb/pragmatic-bacardi-social-share
 */

/* Bacardi Social Share plugin
*
*/

add_action( 'init', 'enqueue_styles' );

function enqueue_styles()
{
	wp_enqueue_style( 'pragmatic-bacardi-social-share-style', '/wp-content/plugins/pragmatic-bacardi-social-share/style.css' );
}

function register_social_share_settings() 
{
	register_setting( 'bacardi_social_share', 'bacardi_social_share_settings' ); 

	add_settings_section( 
		'bacardi_social_global',
		__( 'Bacardi Social Share: Global Options', 'pragmatic-bacardi-social-share' ),
		'bacardi_global_settings_cb',
		'bacardi_social_share'
		);

	add_settings_field(
		'bacardi_icon_size',
		__( 'Icon Size', 'pragmatic-bacardi-social-share' ),	
		'bacardi_icon_size_cb',
		'bacardi_social_share',
		'bacardi_social_global'	
		);

	add_settings_field(
		'bacardi_icon_style',
		__( 'Icon Style', 'pragmatic-bacardi-social-share' ),
		'bacardi_icon_style_cb',
		'bacardi_social_share',
		'bacardi_social_global'
		);

	add_settings_field(
		'bacardi_default_text',
		__( 'Default Text', 'pragmatic-bacardi-social-share' ),
		'bacardi_default_text_cb',
		'bacardi_social_share',
		'bacardi_social_global'
		);

	add_settings_field(
		'bacardi_display_facebook',
		__( 'Display Facebook?', 'pragmatic-bacardi-social-share' ),
		'bacardi_disp_facebook_cb',
		'bacardi_social_share',
		'bacardi_social_global'
		);

	add_settings_field(
		'bacardi_display_twitter',
		__( 'Display Twitter?', 'pragmatic-bacardi-social-share' ),
		'bacardi_disp_twitter_cb',
		'bacardi_social_share',
		'bacardi_social_global'
		);

	add_settings_field(
		'bacardi_display_pinterest',
		__( 'Display Pinterest?', 'pragmatic-bacardi-social-share' ),
		'bacardi_disp_pinterest_cb',
		'bacardi_social_share',
		'bacardi_social_global'
		);
} 
add_action( 'bacardi_admin_init', 'register_social_share_settings' );

function add_menu_items() 
{
	add_submenu_page(
		'bacardi_top_menu',
		'Bacardi Social Share Settings',
		'Bacardi Social Share',
		'manage_options',
		'bacardi_social_share',
		'social_share_settings_page'
		);
}

add_action( 'bacardi_admin_menu', 'add_menu_items' );

function social_share_settings_page()
{
	if ( ! current_user_can( 'manage_options' ) )
	{
		wp_die( __( 'You do not have sufficient permissions to access this page' ) );
	}

	echo '<form action="options.php" method="post">';

	settings_fields( 'bacardi_social_share' );
	do_settings_sections( 'bacardi_social_share' );
	submit_button();

	echo '</form>';
}

function bacardi_global_settings_cb( ) 
{
	echo __( '', 'pragmatic-bacardi-social-share' );
}

function bacardi_icon_size_cb( )
{
	$options = get_option( 'bacardi_social_share_settings' );
	if ( ! isset( $options['bacardi_icon_size'] ) )
	{
		$options['bacardi_icon_size'] = 'medium';
	}

	echo '<select name="bacardi_social_share_settings[bacardi_icon_size]">';
	echo '<option value="small"'; echo ('small' == $options['bacardi_icon_size']) ? ' selected' : ''; echo '>Small</option>';
	echo '<option value="medium"'; echo ('medium' == $options['bacardi_icon_size']) ? ' selected' : ''; echo '>Medium</option>';
	echo '<option value="large"'; echo ('large' == $options['bacardi_icon_size']) ? ' selected' : ''; echo '>Large</option>';
	echo '</select>';
}

function bacardi_icon_style_cb()
{
	$options = get_option( 'bacardi_social_share_settings' );
	if ( ! isset( $options['bacardi_icon_style'] ) )
	{
		$options['bacardi_icon_style'] = 'normal';
	}
	echo '<select name="bacardi_social_share_settings[bacardi_icon_style]">';
	echo '<option value="normal"'; echo ('normal' == $options['bacardi_icon_style']) ? ' selected' : ''; echo '>Not Solid, No Circle</option>';
	echo '<option value="solid"'; echo ('solid' == $options['bacardi_icon_style']) ? ' selected' : ''; echo '>Solid, No Circle</option>';
	echo '<option value="circle"'; echo ('circle' == $options['bacardi_icon_style']) ? ' selected' : ''; echo '>Not Solid, Circle</option>';
	echo '<option value="solidcircle"'; echo ('solidcircle' == $options['bacardi_icon_style']) ? ' selected' : ''; echo '>Solid, Circle</option>';
	echo '</select>';
}

function bacardi_default_text_cb()
{
	$options = get_option( 'bacardi_social_share_settings' );

	echo '<input type="text" name="bacardi_social_share_settings[bacardi_default_text]" value="' . $options['bacardi_default_text'] . '">';
}

function bacardi_disp_facebook_cb()
{
	$options = get_option( 'bacardi_social_share_settings' );
	if ( ! isset( $options['bacardi_display_facebook'] ) )
	{
		$options['bacardi_display_facebook'] = false;
	}

	echo '<input type="checkbox" value=1 name="bacardi_social_share_settings[bacardi_display_facebook]"';
	checked( $options['bacardi_display_facebook'] );
	echo '>';
}

function bacardi_disp_twitter_cb()
{
	$options = get_option( 'bacardi_social_share_settings' );
	if ( ! isset( $options['bacardi_display_twitter'] ) )
	{
		$options['bacardi_display_twitter'] = false;
	}

	echo '<input type="checkbox" value=1 name="bacardi_social_share_settings[bacardi_display_twitter]"';
	checked( $options['bacardi_display_twitter'] );
	echo '>';
}

function bacardi_disp_pinterest_cb()
{
	$options = get_option( 'bacardi_social_share_settings' );
	if ( ! isset( $options['bacardi_display_pinterest'] ) )
	{
		$options['bacardi_display_pinterest'] = false;
	}

	echo '<input type="checkbox" value=1 name="bacardi_social_share_settings[bacardi_display_pinterest]"';
	checked( $options['bacardi_display_pinterest'] );
	echo '>';
}

add_shortcode( 'bacardi_social_share','bacardi_social_share' );

function bacardi_social_share( $atts )
{
	$options = get_option( 'bacardi_social_share_settings' );
	$buttons = '';

	foreach ( $options as $key => $option )
	{
		$new_key = str_replace( 'bacardi_', '', $key );
		$options[$new_key] = $option;
		unset( $options[$key] );
	}

	$url =  "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

	$atts = shortcode_atts($options, $atts);

	if ( isset( $atts['text'] ) )
	{
		$text = $atts['text'];
	}
	else
	{
		$text = $atts['default_text'];
	}

	foreach ( $atts as $key => $val )
	{
		if ( stristr( $key, 'display_') )
		{
			$network = str_replace( 'display_', '', $key );
			$args = array('network' => $network, 'style' => $atts['icon_style'], 'size' => $atts['icon_size'], 'page_url' =>  $url, 'text' => $text );
			$buttons .= generate_button($args);
		}
	}

	return $buttons;
}

function generate_button( $args = array() )
{

	if ( 'facebook' == $args['network'] )
	{
		$href = 'https://facebook.com/sharer/sharer.php?u=' . urlencode($args['page_url']);
	}
	elseif ( 'twitter' == $args['network'] )
	{
		$href = 'https://twitter.com/intent/tweet/?text=' . urlencode($args['text']) . '&url= ' . urlencode($args['page_url']);
	}
	elseif ( 'pinterest' == $args['network'] )
	{
		$href = 'https://pinterest.com/pin/create/button/?url=' . urlencode($args['page_url']) . '&amp;media=' . urlencode($args['page_url']) . '&amp;summary=' . urlencode($args['text']);
	}

	$html = '<a class="resp-sharing-button__link" href="' . $href . '" target="_blank" aria-label="">';
	$html .= '<div class="resp-sharing-button resp-sharing-button--' . $args['network'] . ' resp-sharing-button--' . $args['size'] .'"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--' . $args['style'] . '">';
	$html .= get_svg_code( array('network' => $args['network'], 'style' => $args['style'] ) );
	$html .= '</div>';
	if ( 'medium' == $args['size'] )
	{
		$html .= ucfirst( $args['network'] );
	}
	elseif ( 'large' == $args['size'] )
	{
		$html .= 'Share on ' . ucfirst( $args['network'] );
	}
	$html .= '</div></a>';

	return $html;

}

function get_svg_code ( $args )
{
	$network = $args['network'];
	$style = $args['style'];

	$array_of_svg_codes = array(

		'facebook' => array(
			'normal' => 
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
			        <path stroke-width="2px" stroke-linejoin="round" stroke-miterlimit="10" d="M18.768 7.5h-4.268v-1.905c0-.896.594-1.105 1.012-1.105h2.988v-3.942l-4.329-.013c-3.927 0-4.671 2.938-4.671 4.82v2.145h-3v4h3v12h5v-12h3.851l.417-4z" fill="none"/>
			    </svg>',
			'solid' => 
				'<svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
			        <g>
			            <path d="M18.768,7.465H14.5V5.56c0-0.896,0.594-1.105,1.012-1.105s2.988,0,2.988,0V0.513L14.171,0.5C10.244,0.5,9.5,3.438,9.5,5.32 v2.145h-3v4h3c0,5.212,0,12,0,12h5c0,0,0-6.85,0-12h3.851L18.768,7.465z"/>
			        </g>
			    </svg>',
			'circle' => 
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
			        <g stroke-width="2px" stroke-linejoin="round" stroke-miterlimit="10" fill="none">
			            <circle stroke-linecap="round" cx="12" cy="12" r="11.5"/>
			            <path d="M15.839 9.5h-2.339v-1.022c0-.524.348-.647.592-.647h1.408v-2.307l-2.351-.007c-2.298 0-2.649 1.719-2.649 2.819v1.164h-2v2h2v7h3v-7h2.095l.244-2z"/>
			        </g>
			    </svg>',
			'solidcircle' => 
				'<svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
			        <path d="M12,0C5.383,0,0,5.383,0,12s5.383,12,12,12s12-5.383,12-12S18.617,0,12,0z M15.595,11.5H13.5c0,3.013,0,7,0,7h-3 c0,0,0-3.951,0-7h-2v-2h2V8.336c0-1.1,0.352-2.819,2.649-2.819L15.5,5.524V7.83c0,0-1.163,0-1.408,0 c-0.244,0-0.592,0.124-0.592,0.647V9.5h2.339L15.595,11.5z"/>
			    </svg>',

			),
		'twitter' => array(
			'normal' =>
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
			        <path stroke-width="2px" stroke-linejoin="round" stroke-miterlimit="10" d="M23.407 4.834c-.814.363-1.5.375-2.228.016.938-.562.981-.957 1.32-2.019-.878.521-1.851.9-2.886 1.104-.827-.882-2.009-1.435-3.315-1.435-2.51 0-4.544 2.036-4.544 4.544 0 .356.04.703.117 1.036-3.776-.189-7.125-1.998-9.366-4.748-.391.671-.615 1.452-.615 2.285 0 1.577.803 2.967 2.021 3.782-.745-.024-1.445-.228-2.057-.568l-.001.057c0 2.202 1.566 4.038 3.646 4.456-.666.181-1.368.209-2.053.079.579 1.804 2.257 3.118 4.245 3.155-1.944 1.524-4.355 2.159-6.728 1.881 2.012 1.289 4.399 2.041 6.966 2.041 8.358 0 12.928-6.924 12.928-12.929l-.012-.588c.886-.64 1.953-1.237 2.562-2.149z" />
			    </svg>',
			'solid' =>
			    '<svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
			        <g>
			            <path d="M23.444,4.834c-0.814,0.363-1.5,0.375-2.228,0.016c0.938-0.562,0.981-0.957,1.32-2.019c-0.878,0.521-1.851,0.9-2.886,1.104 C18.823,3.053,17.642,2.5,16.335,2.5c-2.51,0-4.544,2.036-4.544,4.544c0,0.356,0.04,0.703,0.117,1.036 C8.132,7.891,4.783,6.082,2.542,3.332C2.151,4.003,1.927,4.784,1.927,5.617c0,1.577,0.803,2.967,2.021,3.782 C3.203,9.375,2.503,9.171,1.891,8.831C1.89,8.85,1.89,8.868,1.89,8.888c0,2.202,1.566,4.038,3.646,4.456 c-0.666,0.181-1.368,0.209-2.053,0.079c0.579,1.804,2.257,3.118,4.245,3.155C5.783,18.102,3.372,18.737,1,18.459 C3.012,19.748,5.399,20.5,7.966,20.5c8.358,0,12.928-6.924,12.928-12.929c0-0.198-0.003-0.393-0.012-0.588 C21.769,6.343,22.835,5.746,23.444,4.834z"/>
			        </g>
			    </svg>',
			'circle' =>
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
			        <g stroke-width="2px" stroke-linejoin="round" stroke-miterlimit="10">
			            <path d="M18.5 7.354s-1.351.146-1.95.266c-.48-.513-1.164-.832-1.921-.832-1.454 0-2.633 1.179-2.633 2.632 0 .205.023.408.068.599-2.187-.108-4.126-1.157-5.424-2.75-.227.39-.357.841-.357 1.324 0 .914.465 1.718 1.171 2.19-.431-.014-.837-.132-1.192-.33v.035c0 1.274.907 2.338 2.111 2.579-.386.106-.792.121-1.188.046.335 1.045 1.307 1.806 2.458 1.829-1.125.882-2.522 1.25-3.896 1.089 1.165.747 2.549 1.184 4.035 1.184 4.841 0 7.488-4.012 7.488-7.489l-.008-.34c.514-.373.885-1.504 1.238-2.032z"/>
			            <circle stroke-linecap="round" cx="12" cy="12" r="11.5"/>
			        </g>
			    </svg>',
			'solidcircle' =>
				'<svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
			        <path d="M12,0C5.383,0,0,5.383,0,12s5.383,12,12,12s12-5.383,12-12S18.617,0,12,0z M17.262,9.385 c0.006,0.113,0.008,0.226,0.008,0.34c0,3.478-2.647,7.489-7.488,7.489c-1.486,0-2.87-0.437-4.035-1.184 c1.374,0.161,2.771-0.207,3.896-1.089c-1.151-0.022-2.123-0.783-2.458-1.829c0.396,0.075,0.803,0.061,1.188-0.046 c-1.204-0.24-2.111-1.305-2.111-2.579c0-0.011,0-0.023,0-0.035c0.355,0.197,0.762,0.315,1.192,0.33 c-0.706-0.473-1.171-1.277-1.171-2.19c0-0.482,0.13-0.934,0.356-1.324c1.298,1.593,3.237,2.642,5.425,2.75 c-0.045-0.191-0.068-0.394-0.068-0.599c0-1.454,1.179-2.632,2.633-2.632c0.757,0,1.44,0.319,1.921,0.832 c0.6-0.119,1.95-0.266,1.95-0.266C18.147,7.882,17.776,9.013,17.262,9.385z"/>
			    </svg>',
			),
		'pinterest' => array(
			'normal' =>
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
        			<path stroke-width="2px" stroke-linejoin="round" stroke-miterlimit="10" d="M12.137.5c-6.277 0-9.442 4.5-9.442 8.252 0 2.272.86 4.295 2.705 5.047.303.124.574.004.661-.33l.271-1.061c.088-.331.055-.446-.19-.736-.532-.626-.872-1.439-.872-2.59 0-3.339 2.498-6.328 6.505-6.328 3.548 0 5.497 2.168 5.497 5.063 0 3.809-1.687 7.024-4.189 7.024-1.382 0-2.416-1.142-2.085-2.545.397-1.675 1.167-3.479 1.167-4.688 0-1.081-.58-1.983-1.782-1.983-1.413 0-2.548 1.461-2.548 3.42 0 1.247.422 2.09.422 2.09s-1.445 6.126-1.699 7.199c-.505 2.137-.076 4.756-.04 5.02.021.157.224.195.314.078.13-.171 1.813-2.25 2.385-4.325.162-.589.929-3.632.929-3.632.459.876 1.801 1.646 3.228 1.646 4.247 0 7.128-3.871 7.128-9.053-.002-3.918-3.32-7.568-8.365-7.568z" />
    			</svg>',
    		'solid' =>
    			'<svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
			        <path d="M12.137,0.5C5.86,0.5,2.695,5,2.695,8.752c0,2.272,0.8 ,4.295,2.705,5.047c0.303,0.124,0.574,0.004,0.661-0.33 c0.062-0.231,0.206-0.816,0.271-1.061c0.088-0.331,0.055-0.446-0.19-0.736c-0.532-0.626-0.872-1.439-0.872-2.59 c0-3.339,2.498-6.328,6.505-6.328c3.548,0,5.497,2.168,5.497,5.063c0,3.809-1.687,7.024-4.189,7.024 c-1.382,0-2.416-1.142-2.085-2.545c0.397-1.675,1.167-3.479,1.167-4.688c0-1.081-0.58-1.983-1.782-1.983 c-1.413,0-2.548,1.461-2.548,3.42c0,1.247,0.422,2.09,0.422,2.09s-1.445,6.126-1.699,7.199c-0.505,2.137-0.076,4.756-0.04,5.02 c0.021,0.157,0.224,0.195,0.314,0.078c0.13-0.171,1.813-2.25,2.385-4.325c0.162-0.589,0.929-3.632,0.929-3.632 c0.459,0.876,1.801,1.646,3.228,1.646c4.247,0,7.128-3.871,7.128-9.053C20.5,4.15,17.182,0.5,12.137,0.5z"/>
			    </svg>',
			'circle' =>
				'<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
			        <g stroke-width="2px" stroke-linejoin="round" stroke-miterlimit="10">
			            <circle stroke-linecap="round" cx="12" cy="12" r="11.5"/><path d="M8.003 11.215c-.157-.342-.249-.748-.249-1.246 0-2.32 1.738-4.4 4.525-4.4 2.468 0 3.824 1.509 3.824 3.521 0 2.649-1.173 4.888-2.914 4.888-.961 0-1.682-.796-1.451-1.772.277-1.163.813-2.42.813-3.261 0-.752-.404-1.378-1.24-1.378-.982 0-1.772 1.016-1.772 2.378 0 .866.294 1.454.294 1.454l-1.183 5.008c-.351 1.487-.053 3.309-.027 3.492.015.11.154.137.218.053.091-.118 1.262-1.563 1.66-3.007.111-.409.646-2.526.646-2.526.318.609 1.252 1.145 2.244 1.145 2.955 0 4.959-2.693 4.959-6.297 0-2.728-2.309-5.267-5.819-5.267-4.366 0-6.568 3.13-6.568 5.74 0 .846.172 1.643.527 2.279l1.513-.804z"/>
			        </g>
			    </svg>',
			'solidcircle' =>
				'    <svg version="1.1" x="0px" y="0px" width="24px" height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
				        <path d="M12,0C5.383,0,0,5.383,0,12s5.383,12,12,12s12-5.383,12-12S18.617,0,12,0z M13.391,15.563c-0.992,0-1.926-0.535-2.244-1.145c0,0-0.535,2.117-0.646,2.526c-0.398,1.443-1.569,2.889-1.66,3.007c-0.063,0.084-0.203,0.058-0.218-0.053 c-0.025-0.184-0.323-2.005,0.027-3.492c0.176-0.746,1.183-5.008,1.183-5.008s-0.294-0.588-0.294-1.454 c0-1.362,0.79-2.378,1.772-2.378c0.836,0,1.24,0.626,1.24,1.378c0,0.841-0.535,2.098-0.813,3.261 c-0.23,0.977,0.49,1.772,1.451,1.772c1.741,0,2.914-2.238,2.914-4.888c0-2.013-1.356-3.521-3.824-3.521 c-2.787,0-4.525,2.08-4.525,4.4c0,0.498,0.092,0.904,0.249,1.246L6.49,12.02c-0.355-0.637-0.527-1.434-0.527-2.279 C5.963,7.13,8.165,4,12.531,4c3.51,0,5.818,2.539,5.818,5.266C18.35,12.869,16.346,15.563,13.391,15.563z"/>
				    </svg>',
			),
	);

	return $array_of_svg_codes[$network][$style];
}